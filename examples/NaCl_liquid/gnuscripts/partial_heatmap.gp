res
set term postscript eps size 11.5,5.0 enh col "Helvetica" 22

wMax=50

data_Cl="data/current_longitudinal_heatmap_"
data_Ct="data/current_transverse_heatmap_"

filename='eps/NaCl_partial_heatmap.eps'
set out filename
#set enc iso_8859_1

# Colorbar
# =====================
set palette color
set palette model RGB
#set palette defined ( \
  20   '#ffffcc', \
  30   '#a1dab4', \
  40   '#41b6c4', \
  50   '#2c7fb8', \
  80   '#253494' )
set palette defined ( \
  20   '#d7191c', \
  30   '#fdae61', \
  40   '#ffffbf', \
  50   '#abdda4', \
  60   '#2b83ba' )

set palette defined( \
20  '#d53e4f', \
30  '#fc8d59', \
40  '#fee08b', \
50  '#e6f598', \
60  '#99d594', \
80  '#3288bd' )

#set palette defined ( \
    0 '#000090', \
    1 '#000fff', \
    2 '#0090ff', \
    3 '#0fffee', \
    4 '#90ff70', \
    5 '#ffee00', \
    6 '#ff7000', \
    7 '#ee0000', \
    8 '#7f0000')

#set cbti 0,0.05
unset cbti

#unset colorbox

set view map
set pm3d map interpolate 1,1
#set cntrparam bspline
#set cntrparam points 10
#set cntrparam levels discrete 0.01,0.03,0.1
#set contour surface

# Plot
# ========================

set multiplot  

titleOff = -0.5
TOP = 0.92
DY  = 0.4
RIGHT=0.96
DX=0.3
dx=0.07 ; dy=0.135 # labels

set lmargin 8
set bmargin at screen TOP-DY
set tmargin at screen TOP
#set size 0.3,0.55

set cbra [-20:45]

set yla 'Frequency (meV)'
set yra[0:wMax]
set xra[2.4:30.0]
unset key
unset xtics
set xtics nomirror
set ytics nomirror
unset xtics

set title "NaNa" off 0,titleOff
set rmargin at screen RIGHT-2*DX
set lmargin at screen RIGHT-3*DX
set obj 99 rect from gr 0,gr 1 to gr dx,gr 1-dy fc rgb "white" fs border -1 front
set lab 99 "(a)" at gr dx/2,gr 1.0-dy/2 center front
splot data_Cl."Na_Na" u 1:2:3 w pm3d


unset yla
unset ytics
set title "ClCl"
set rmargin at screen RIGHT-1*DX
set lmargin at screen RIGHT-2*DX
set obj 99 rect from gr 0,gr 1 to gr dx,gr 1-dy fc rgb "white" fs border -1 front
set lab 99 "(b)" at gr dx/2,gr 1.0-dy/2 center front
splot data_Cl."Cl_Cl" u 1:2:3 w pm3d

set title "NaCl"
set rmargin at screen RIGHT
set lmargin at screen RIGHT-DX
set lab 99 "(c)" at gr dx/2,gr 1-dy/2 center front
splot data_Cl."Cl_Na" u 1:2:3 w pm3d


set cbra [-25:55]
set cbra [-20:45]
unset title
set bmargin at screen TOP-2*DY
set tmargin at screen TOP-DY
set xla 'q (nm^{-1})'
set xtics('0.0' 0.0,'10.0' 10.0,'20.0' 20.0)
set yla 'Frequency (meV)'
set ytics 10
set xtics nomirror
set ytics nomirror

set rmargin at screen RIGHT-2*DX
set lmargin at screen RIGHT-3*DX
set obj 99 rect from gr 0,gr 1 to gr dx,gr 1-dy fc rgb "white" fs border -1 front
set lab 99 "(d)" at gr dx/2,gr 1.0-dy/2 center front
splot data_Ct."Na_Na" u 1:2:3 w pm3d

unset yla
unset ytics
set rmargin at screen RIGHT-1*DX
set lmargin at screen RIGHT-2*DX
set obj 99 rect from gr 0,gr 1 to gr dx,gr 1-dy fc rgb "white" fs border -1 front
set lab 99 "(e)" at gr dx/2,gr 1.0-dy/2 center front
splot data_Ct."Cl_Cl" u 1:2:3 w pm3d

set xtics('0.0' 0.0,'10.0' 10.0,'20.0' 20.0,'30.0' 30.0 )
set rmargin at screen RIGHT
set lmargin at screen RIGHT-DX
set lab 99 "(f)" at gr dx/2,gr 1-dy/2 center front
splot data_Ct."Cl_Na" u 1:2:3 w pm3d



unset multiplot