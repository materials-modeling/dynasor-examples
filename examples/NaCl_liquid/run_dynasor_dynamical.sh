#!/bin/sh


K_MAX=30     # Consider kspace from gamma (0) to K_MAX inverse nanometer
K_BINS=100      # Collect result using K_BINS "bins" between 0 and $K_MAX
MAX_KPOINTS=2000
TIME_WINDOW=1000  # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=20000   # Read at most MAX_FRAMES frames from trajectory file (then stop)

dt=$((10*2)) # This needs to be correspond to lammps timestep * dumpFreq * $STEP.


TRAJECTORY="/scratch/erikfr/data_storage/NaCl_data/dynasor_runs/liquid_size8_T1200/sampling/pos.data.gz"
INDEXFILE="data/index_file"
OUTPUT="outputs/dynsf_out_T1200_dynamical"


dynasor -f "$TRAJECTORY" -n "$INDEXFILE" \
    --k-bins=$K_BINS \
    --k-max=$K_MAX \
    --max-k-points=$MAX_KPOINTS \
    --max-frames=$MAX_FRAMES \
    --nt=$TIME_WINDOW \
    --dt=$dt \
    --op=$OUTPUT.pickle \
    -v






