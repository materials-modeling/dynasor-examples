import numpy as np

index_file = 'data/index_file'
ref_file = '/scratch/erikfr/data_storage/NaCl_data/dynasor_runs/liquid_size8_T1200/sampling/ref.pos'

with open(ref_file, 'r') as f:
    lines = f.readlines()

    # find start
    start = -1
    for n, line in enumerate(lines):
        if line.startswith('ITEM: ATOMS'):
            cols = line[11:].split()
            id_ind = cols.index('id')
            type_ind = cols.index('type')
            start = n + 1
            break

    # loop over atoms
    index_list = []
    for line in lines[start:]:
        flds = line.split()

        atom_index = flds[id_ind]
        atom_type = flds[type_ind]
        index_list.append([int(atom_index), int(atom_type)])
    index_list = np.array(index_list)

# write index file
# type 1 == 'Cl'
# type 2 == 'Na'
with open(index_file, 'w') as f:
    unique_types = sorted(np.unique(index_list[:, 1]))
    assert unique_types == [1, 2]
    for unique, element in zip(unique_types, ['Cl', 'Na']):
        f.write('[ %s ]\n' % element)
        for i, t in index_list:
            if t == unique:
                f.write('%d  ' % i)
        f.write('\n')
