"""
Generate gnuplot heatmap data
"""

import numpy as np
from dynasor_pytools import DynasorAnalyzer
from dynasor_pytools.utils import write_gnuplot_heatmap_data


invfs2mev = 658.2119
wmax = 50.0

m_Cl = 35.45000000
m_Na = 22.98976928
q_Cl = -1.0
q_Na = +1.0

# 0.5 since partials are normalized with N_A and not (N_A+N_B)
norm_charge = 0.5 * (abs(q_Cl) + abs(q_Na))**2
norm_mass = 0.5 * (m_Cl+m_Na)**2
# ---------------------------

dynasor_output = 'outputs/dynsf_out_T1200_dynamical.pickle'
analyzer = DynasorAnalyzer(dynasor_output)

k_vals = analyzer.raw_data_dict['k']
w_vals = analyzer.raw_data_dict['w'] * invfs2mev
w_cut = np.argmin(np.abs(w_vals-wmax))
w_vals = w_vals[:w_cut]

# generate partial
# ------------------

for partial in ['0_0', '0_1', '1_1']:
    Cl_partial = analyzer.raw_data_dict['Cl_k_w_%s' % partial][:w_cut, :]
    Ct_partial = analyzer.raw_data_dict['Ct_k_w_%s' % partial][:w_cut, :]
    S_partial = analyzer.raw_data_dict['S_k_w_%s' % partial][:w_cut, :]

    tmp = partial.replace('0', 'Cl')
    tmp = tmp.replace('1', 'Na')

    write_gnuplot_heatmap_data('data/current_longitudinal_heatmap_%s' % tmp, k_vals, w_vals, Cl_partial)
    write_gnuplot_heatmap_data('data/current_transverse_heatmap_%s' % tmp, k_vals, w_vals, Ct_partial)
    write_gnuplot_heatmap_data('data/dynamical_structurefactor_heatmap_%s' % tmp, k_vals, w_vals, S_partial)


# generate charge and mass
# --------------------------

# Longitudinal
C_Cl_Cl = analyzer.raw_data_dict['Cl_k_w_0_0'][:w_cut, :]
C_Cl_Na = analyzer.raw_data_dict['Cl_k_w_0_1'][:w_cut, :]
C_Na_Na = analyzer.raw_data_dict['Cl_k_w_1_1'][:w_cut, :]

C_charge = (q_Cl**2 * C_Cl_Cl + q_Na**2 * C_Na_Na + 2 * q_Cl*q_Na * C_Cl_Na) / norm_charge
C_mass =   (m_Cl**2 * C_Cl_Cl + m_Na**2 * C_Na_Na + 2 * m_Cl*m_Na * C_Cl_Na) / norm_mass

write_gnuplot_heatmap_data('data/current_longitudinal_heatmap_charge', k_vals, w_vals, C_charge)
write_gnuplot_heatmap_data('data/current_longitudinal_heatmap_mass', k_vals, w_vals, C_mass)

# Transverse
C_Cl_Cl = analyzer.raw_data_dict['Ct_k_w_0_0'][:w_cut, :]
C_Cl_Na = analyzer.raw_data_dict['Ct_k_w_0_1'][:w_cut, :]
C_Na_Na = analyzer.raw_data_dict['Ct_k_w_1_1'][:w_cut, :]

C_charge = (q_Cl**2 * C_Cl_Cl + q_Na**2 * C_Na_Na + 2 * q_Cl*q_Na * C_Cl_Na) / norm_charge
C_mass =   (m_Cl**2 * C_Cl_Cl + m_Na**2 * C_Na_Na + 2 * m_Cl*m_Na * C_Cl_Na) / norm_mass

write_gnuplot_heatmap_data('data/current_transverse_heatmap_charge', k_vals, w_vals, C_charge)
write_gnuplot_heatmap_data('data/current_transverse_heatmap_mass', k_vals, w_vals, C_mass)