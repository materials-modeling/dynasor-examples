import numpy as np
from dynasor_pytools import DynasorAnalyzer
import matplotlib.pyplot as plt
from mplpub import setup, tableau


m_Cl = 35.45000000
m_Na = 22.98976928
q_Cl = -1.0
q_Na = +1.0

# --------------
dynasor_output = 'outputs/dynsf_out_T1200_static.pickle'
analyzer = DynasorAnalyzer(dynasor_output)

# -----------------------

k = analyzer.raw_data_dict['k']
time = analyzer.raw_data_dict['t']
assert abs(time[0]) < 1e-6


Fkt_Cl_Cl = analyzer.raw_data_dict['F_k_t_0_0']
Fkt_Cl_Na = analyzer.raw_data_dict['F_k_t_0_1']
Fkt_Na_Na = analyzer.raw_data_dict['F_k_t_1_1']
Sq_Cl_Cl = Fkt_Cl_Cl[0, :]
Sq_Cl_Na = Fkt_Cl_Na[0, :]
Sq_Na_Na = Fkt_Na_Na[0, :]


# 0.5 since partials are normalized with N_A and not (N_A+N_B)
norm_charge = 0.5 * (abs(q_Cl) + abs(q_Na))**2
norm_mass = 0.5 * (m_Cl+m_Na)**2
Sq_charge = (q_Cl**2 * Sq_Cl_Cl + q_Na**2 * Sq_Na_Na + 2 * q_Cl*q_Na * Sq_Cl_Na) / norm_charge
Sq_mass = (m_Cl**2 * Sq_Cl_Cl + m_Na**2 * Sq_Na_Na + 2 * m_Cl*m_Na * Sq_Cl_Na) / norm_mass

# save data
header = 'Col 1: q (2pi * inverse nm)\nCol 2: S_ClCl(q)\nCol 3: S_NaCl(q)' \
         '\nCol 4: S_NaNa(q)\nCol 5: S_charge(q)\nCol 6: S_mass(q)'
np.savetxt('data/structure_factor_static', np.stack((k, Sq_Cl_Cl, Sq_Cl_Na, Sq_Na_Na, Sq_charge, Sq_mass), axis=1), header=header)

# Plotting
# ------------------
extra_settings = {'legend.frameon': True,
                  'legend.fancybox': True,
                  'legend.framealpha': 0.8,
                  'legend.edgecolor': '0.8'}
setup(extra_settings=extra_settings)
fig = plt.figure(figsize=(6.8, 4.8))
lw = 2.0
fs = 16

# ----------
plt.plot(k[1:], Sq_charge[1:], label='charge', color=tableau['blue'], linewidth=lw)
plt.plot(k[1:], Sq_mass[1:], label='mass', color=tableau['red'], linewidth=lw)

plt.xlabel('q (nm$^{-1}$)', fontsize=fs)
plt.xlim([0.0, 100.0])
plt.legend(fontsize=fs, loc=1)
plt.ylabel('$S(q)$', fontsize=fs)
plt.ylim(bottom=0.0)
plt.gca().tick_params(labelsize=fs)

plt.tight_layout()
plt.savefig('eps/NaCl_Sq.eps')
plt.show()
