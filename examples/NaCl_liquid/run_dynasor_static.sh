#!/bin/sh


K_MAX=100      # Consider kspace from gamma (0) to K_MAX inverse nanometer
K_BINS=500      # Collect result using K_BINS "bins" between 0 and $K_MAX
MAX_FRAMES=20000   # Read at most MAX_FRAMES frames from trajectory file (then stop)
K_POINTS=5000


TRAJECTORY="/scratch/erikfr/data_storage/NaCl_data/dynasor_runs/liquid_size8_T1200/sampling/pos.data.gz"
INDEXFILE="data/index_file"
OUTPUT="outputs/dynsf_out_T1200_static"


dynasor -f "$TRAJECTORY"  -n "$INDEXFILE" \
    --k-bins=$K_BINS \
    --k-max=$K_MAX \
    --max-frames=$MAX_FRAMES \
    --max-k-points=$K_POINTS \
    --op=$OUTPUT.pickle \
    -v



