import numpy as np
import matplotlib.pyplot as plt
from mplpub import setup, tableau
from dynasor_pytools.fft_helpers import filon_fft

invps2meV = 4.13567


# parameters
window = 2000
max_frames = 300000


# read data
data_fname = 'data/Al_vacf_{}_T{}_window{}_maxframes{}'
data300 = np.loadtxt(data_fname.format('solid', 300, window, max_frames))
data900 = np.loadtxt(data_fname.format('solid', 900, window, max_frames))
data1200 = np.loadtxt(data_fname.format('liquid', 1200, window, max_frames))

#   harmonic300 = np.loadtxt('data/Al_harmonic_dos_T300')
#harmonic900 = np.loadtxt('data/Al_harmonic_dos_T900')

w300, dos300 = filon_fft(data300[:, 0], data300[:, 1], 'fermi-dirac', t_0=0.9, t_width=0.15)
w900, dos900 = filon_fft(data900[:, 0], data900[:, 1], 'fermi-dirac', t_0=0.9, t_width=0.15)
w1200, dos1200 = filon_fft(data1200[:, 0], data1200[:, 1], 'fermi-dirac', t_0=0.9, t_width=0.15)

w300 *= invps2meV / (2 * np.pi)
w900 *= invps2meV / (2 * np.pi)
w1200 *= invps2meV / (2 * np.pi)


# plot
extra_settings = {'legend.frameon': True,
                  'legend.fancybox': True,
                  'legend.framealpha': 0.8,
                  'legend.edgecolor': '0.8'}
setup(extra_settings=extra_settings)
fig = plt.figure(figsize=(6.8, 4.8))
fs = 16
lw = 2.0

plt.plot(w300, dos300, '-', lw=lw, color=tableau['blue'], label='300K (solid)')
plt.plot(w900, dos900, '-', lw=lw, color=tableau['orange'], label='900K (solid)')
plt.plot(w1200, dos1200, '-', lw=lw, color=tableau['red'], label='1200K (liquid)')


plt.xlim([0.0, 45.0])
plt.ylim([0.0, 0.13])

plt.xlabel('Frequency (meV)', fontsize=fs)
plt.ylabel('DOS $g(\omega)$', fontsize=fs)
plt.gca().tick_params(labelsize=fs)

plt.legend(loc=2, fontsize=fs)
plt.tight_layout()
plt.savefig('eps/Al_vacf.eps')
plt.show()
