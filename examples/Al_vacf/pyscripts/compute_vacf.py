import numpy as np
from lammps_tools.correlation_functions import compute_vacf


# parameters
window = 2000
max_frames = 300000
dt = 0.002   # 2 fs

header = 'Col 1: time (ps)\nCol 2: vacf(t)'

# filenames
liquid_fname = '/scratch/erikfr/data_storage/Al_data/dynasor_runs/liquid_size12_T1200/sampling/pos.data.gz'
solid300_fname = '/scratch/erikfr/data_storage/Al_data/dynasor_runs/size12_T300/pos.data.gz'
solid900_fname = '/scratch/erikfr/data_storage/Al_data/dynasor_runs/size12_T900/pos.data.gz'
output_fname = 'data/Al_vacf_{}_T{}_window{}_maxframes{}'

# liquid 1200K
if False:
    time, vacf = compute_vacf(liquid_fname, window, max_frames, dt)
    np.savetxt(output_fname.format('liquid', 1200, window, max_frames), np.vstack((time, vacf)).T, header=header)

# solid 300K
if False:
    time, vacf = compute_vacf(solid300_fname, window, max_frames, dt)
    np.savetxt(output_fname.format('solid', 300, window, max_frames), np.vstack((time, vacf)).T, header=header)

# solid 900K
if True:
    time, vacf = compute_vacf(solid900_fname, window, max_frames, dt)
    np.savetxt(output_fname.format('solid', 900, window, max_frames), np.vstack((time, vacf)).T, header=header)

