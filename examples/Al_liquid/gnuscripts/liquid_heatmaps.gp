res
set term postscript eps size 11.5,3.5 enh col "Helvetica" 22
#set term png



data1="data/dynamical_structurefactor_heatmap"
data2="data/current_longitudinal_heatmap"
data3="data/current_transverse_heatmap"
filename='eps/Al_liquid_heatmap.eps'
set out filename
set enc iso_8859_1


# Colorbar
# =====================

set palette color
set palette model RGB
set palette defined ( \
  20   '#ffffcc', \
  30   '#a1dab4', \
  40   '#41b6c4', \
  50   '#2c7fb8', \
  80   '#253494' )

#v1
#set palette defined ( \
  20   '#d7191c', \
  35   '#fdae61', \
  45   '#ffffbf', \
  60   '#abdda4', \
  80   '#2b83ba' )
#set palette negative

#v2
#set palette defined( \
	20	'#feebe2', \
30 '#fbb4b9', \
40 '#f768a1', \
50 '#c51b8a', \
80 '#7a0177' )

#v3
#set palette defined(\
20 '#edf8fb', \
30 '#b3cde3', \
40 '#8c96c6', \
50 '#8856a7', \
80 '#810f7c')

# v4
#set palette gray



#set cbti 0,0.05
unset cbti

#unset colorbox

set view map
set pm3d map interpolate 1,1
#set cntrparam bspline
#set cntrparam points 10
#set cntrparam levels discrete 0.01,0.03,0.1
#set contour surface


set xla 'q (nm^{-1})'  off 0.0,0.5
set yla 'Frequency (meV)'
set yra[0.0:50]
set xra[2.4: 20.0]
set xtics('0' 0.0,'5' 5.0,'10' 10.0,'15' 15.0)
set xtics nomirror
set ytics nomirror
unset key

# Plot
# ========================
set multiplot  
set lmargin 8
set bmargin 0.5
set tmargin 0.0

set size 0.3,1.16
#set offset 0,0,graph 0.05, graph 0.05

RIGHT=0.96
DX=0.3

dx=0.07 ; dy=0.085


unset colorbox
set obj 99 rect from gr 0,gr 1 to gr dx,gr 1-dy fc rgb "white" fs border -1 front
set lab 99 "(a)" at gr dx/2,gr 1-dy/2 center front
set cbra [0:6]
set rmargin at screen RIGHT-2*DX
set lmargin at screen RIGHT-3*DX
set tmargin 0.0
splot data1 u 1:2:3 w pm3d

set lab 99 "(b)" at gr dx/2,gr 1-dy/2 center front
set cbra [0:80]
unset yla
unset ytics
set rmargin at screen RIGHT-1*DX
set lmargin at screen RIGHT-2*DX
splot data2 u 1:2:3 w pm3d

set colorbox
set xtics('0' 0.0,'5' 5.0,'10' 10.0,'15' 15.0,'20' 20.0)
set lab 99 "(c)" at gr dx/2,gr 1-dy/2 center front
set rmargin at screen RIGHT
set lmargin at screen RIGHT-DX
splot data3 u 1:2:3 w pm3d


unset multiplot
