import numpy as np
import matplotlib.pyplot as plt
from dynasor_pytools import DynasorCurrentAnalyzer
import mplpub


dynasor_output = 'outputs/dynsf_out_T1200_static.pickle'
analyzer = DynasorCurrentAnalyzer(dynasor_output)

# --------------

k = analyzer.raw_data_dict['k']
time = analyzer.raw_data_dict['t']
assert abs(time[0]) < 1e-6

Fkt = analyzer.raw_data_dict['F_k_t_0_0']
Sq = Fkt[0, :]

# save data
header = 'Col 1: q (2pi * inverse nm)\nCol 2: S(q)'
np.savetxt('data/structure_factor_static', np.stack((k, Sq), axis=1), header=header)

# Plotting
# ------------------
mplpub.setup('natcom', width=3.5, height=2.7)
fig = plt.figure()

plt.plot(k[1:], Sq[1:], color=mplpub.tableau['blue'])
plt.xlim([0.0, 100.0])
plt.ylim(bottom=0.0)
plt.xlabel('q (nm$^{-1}$)')
plt.ylabel('S(q)')

plt.tight_layout()
plt.savefig('pdf/Al_liquid_Sq.pdf')
plt.show()
