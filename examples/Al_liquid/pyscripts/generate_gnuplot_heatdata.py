"""
Generate gnuplot heatmap data
"""

import numpy as np
from dynasor_pytools import DynasorAnalyzer
from dynasor_pytools.utils import write_gnuplot_heatmap_data

invfs2mev = 658.2119
wmax = 60.0


dynasor_output = 'outputs/dynsf_out_T1200_dynamical.pickle'
analyzer = DynasorAnalyzer(dynasor_output)

k_vals = analyzer.raw_data_dict['k']
w_vals = analyzer.raw_data_dict['w'] * invfs2mev
w_cut = np.argmin(np.abs(w_vals-wmax))
w_vals = w_vals[:w_cut]

Cl = analyzer.raw_data_dict['Cl_k_w_0_0'][:w_cut, :]
Ct = analyzer.raw_data_dict['Ct_k_w_0_0'][:w_cut, :]
S = analyzer.raw_data_dict['S_k_w_0_0'][:w_cut, :]

write_gnuplot_heatmap_data('data/dynamical_structurefactor_heatmap', k_vals, w_vals, S)
write_gnuplot_heatmap_data('data/current_longitudinal_heatmap', k_vals, w_vals, Cl)
write_gnuplot_heatmap_data('data/current_transverse_heatmap', k_vals, w_vals, Ct)
