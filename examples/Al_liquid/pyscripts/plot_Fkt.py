"""
Plot example Fkt Skw
"""

import numpy as np
from dynasor_pytools import DynasorCurrentAnalyzer
import matplotlib.pyplot as plt
import mplpub
from dynasor_pytools.fft_helpers import filon_fft

invfs2mev = 658.2119


# parameters
wmax = 50.0
tmax = 5000

k1 = 5.0
k2 = 10.0

dynasor_output = 'outputs/dynsf_out_T1200_dynamical.pickle'

# collect data
analyzer = DynasorCurrentAnalyzer(dynasor_output)

k = analyzer.raw_data_dict['k']
t = analyzer.raw_data_dict['t']
w = analyzer.raw_data_dict['w'] * invfs2mev

kind1 = np.argmin(np.abs(k - k1))
kind2 = np.argmin(np.abs(k - k2))
t_cut = np.argmin(np.abs(t - tmax))
t = t[:t_cut]
w_cut = np.argmin(np.abs(w-wmax))
w = w[:w_cut]

Fkt = analyzer.raw_data_dict['F_k_t_0_0'][:t_cut, :]
Skw = analyzer.raw_data_dict['S_k_w_0_0'][:w_cut, :]

# FFT
w_filon1, Skw_filon1 = filon_fft(t, Fkt[:, kind1], fft_window='fermi-dirac', t_0=1000, t_width=100)
w_filon2, Skw_filon2 = filon_fft(t, Fkt[:, kind2], fft_window='fermi-dirac', t_0=1000, t_width=100)
w_filon1 *= invfs2mev
w_filon2 *= invfs2mev

# plotting
mplpub.setup('natcom', width=3.3, height=4.0)
fig = plt.figure()

ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

ax1.plot(t, Fkt[:, kind1], '-', color=mplpub.tableau['red'], label='q = %2.2f nm$^{-1}$' % k[kind1])
ax1.plot(t, Fkt[:, kind2], '-', color=mplpub.tableau['blue'], label='q = %2.2f nm$^{-1}$' % k[kind2])

ax2.plot(w_filon1, Skw_filon1, '-',  color=mplpub.tableau['red'], label='q = %2.2f nm$^{-1}$' % k[kind1])
ax2.plot(w_filon2, Skw_filon2, '-', color=mplpub.tableau['blue'], label='q = %2.2f nm$^{-1}$' % k[kind2])

ax1.set_xlabel('Time (fs)')
ax1.set_ylabel('$F(q,t)$')
ax1.set_xlim([0.0, 1000])

ax2.set_xlabel('$\omega$ (meV)')
ax2.set_ylabel('$S(q,\omega)$')
ax2.set_xlim([0.0, 50.0])
ax2.set_ylim(bottom=0.0)

for ax in [ax1, ax2]:
    ax.legend(loc=1)

x = 0.1
y = 0.85
ax1.text(x, y, 'a)', transform=ax1.transAxes)
ax2.text(x, y, 'b)', transform=ax2.transAxes)

plt.tight_layout()
plt.savefig('pdf/Al_liquid_Fkt_Skw.pdf')
plt.show()
