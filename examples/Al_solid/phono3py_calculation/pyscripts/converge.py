import numpy as np
import matplotlib.pyplot as plt


T = 300
mesh_vals = [12, 24, 36, 48]

fig = plt.figure(figsize=(15, 5))
ax1 = fig.add_subplot(1, 3, 1)
ax2 = fig.add_subplot(1, 3, 2)
ax3 = fig.add_subplot(1, 3, 3)
ms = 4
for mesh, color  in zip(mesh_vals, ['r', 'b', 'g', 'm', 'y']):
    try:
        data_G2X = np.loadtxt('data/phonons_mesh%d%d%d_T%d_G2X' % (mesh, mesh, mesh, T))
        data_G2L = np.loadtxt('data/phonons_mesh%d%d%d_T%d_G2L' % (mesh, mesh, mesh, T))
        data_G2K2X = np.loadtxt('data/phonons_mesh%d%d%d_T%d_G2K2X' % (mesh, mesh, mesh, T))
    except:
        print('didnt find mesh %d' % mesh)
        continue

    k = np.linspace(0, 1, data_G2X.shape[0])
    ax1.plot(k, data_G2X[:, 6], 'o', label='mesh %d' % mesh, color=color, markersize=ms)
    ax2.plot(k, data_G2X[:, 7], 'o', color=color, markersize=ms)
    ax3.plot(k, data_G2X[:, 8], 'o', color=color, markersize=ms)
    
    k = np.linspace(1, 2, data_G2K2X.shape[0])
    ax1.plot(k, data_G2K2X[::-1, 6], 'o', color=color, markersize=ms)
    ax2.plot(k, data_G2K2X[::-1, 7], 'o', color=color, markersize=ms)
    ax3.plot(k, data_G2K2X[::-1, 8], 'o', color=color, markersize=ms)

    k = np.linspace(2, 3, data_G2L.shape[0])
    ax1.plot(k, data_G2L[:, 6], 'o', color=color, markersize=ms)
    ax2.plot(k, data_G2L[:, 7], 'o', color=color, markersize=ms)
    ax3.plot(k, data_G2L[:, 8], 'o', color=color, markersize=ms)

ax1.legend()
plt.tight_layout()
plt.show()