import numpy as np
import h5py


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2, tol=1e-8):
    """ Returns the angle in radians between vectors v1 and v2 """
    if np.linalg.norm(v1) < tol or np.linalg.norm(v2) < tol:
        return -1
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    angle = np.arccos(np.dot(v1_u, v2_u))
    if np.isnan(angle):
        if (v1_u == v2_u).all():
            return 0.0
        else:
            return np.pi
    return angle


THz2meV = 4.13567  # Conversion from THz to meV
tol = 1e-6
mesh = 40
T = 900
file = h5py.File('outputs/kappa-m%d%d%d-T%d.hdf5' % (mesh, mesh, mesh, T))

gamma = file['gamma'][:][0] * THz2meV  # gamma has shape (1, Npoints, 3)
qpoints = file['qpoint'][:]
frequency = file['frequency'][:] * THz2meV
temperatures = file['temperature'][:]

assert abs(temperatures[0] - T) < tol

# fcc reciprocal space
kx = np.array([-1.0, 1.0, 1.0])
ky = np.array([1.0, -1.0, 1.0])
kz = np.array([1.0, 1.0, -1.0])

q_reals = []
for q in qpoints:
    q_reals.append(q[0] * kx + q[1] * ky + q[2] * kz)

# find G2X path
data_G2X = []
for q, f, g in zip(qpoints, frequency, gamma):
    q_real = q[0] * kx + q[1] * ky + q[2] * kz
    if abs(q[0] - q[1]) < tol and abs(q[2]) < tol:
        data_G2X.append(np.hstack((q_real, f, g)))
data_G2X = np.array(data_G2X)


# find G2L path
data_G2L = []
for q, f, g in zip(qpoints, frequency, gamma):
    q_real = q[0] * kx + q[1] * ky + q[2] * kz
    if abs(q[1]) < tol and abs(q[2]) < tol:
        data_G2L.append(np.hstack((q_real, f, g)))
data_G2L = np.array(data_G2L)


# find G2K2X path
data_G2K2X = [[0.0]*9]
for q, f, g in zip(qpoints, frequency, gamma):
    q_real = q[0] * kx + q[1] * ky + q[2] * kz
    if abs(1.0-q[0]%1-q[1]%1) < tol and abs(q[2]) < tol:
        data_G2K2X.append(np.hstack((q_real, f, g)))
data_G2K2X = np.array(data_G2K2X)


# save data
header = 'Col 1-3: kpoint (kx,ky,kz)\nCol 4-6: Frequencies (meV)\nCol 7-9: Gamma (meV)'
np.savetxt('data/phonons_mesh%d%d%d_T%d_G2X' % (mesh, mesh, mesh, T), data_G2X, header=header)
np.savetxt('data/phonons_mesh%d%d%d_T%d_G2L' % (mesh, mesh, mesh, T), data_G2L, header=header)
np.savetxt('data/phonons_mesh%d%d%d_T%d_G2K2X' % (mesh, mesh, mesh, T), data_G2K2X, header=header)

# save plot friendly data
k_scaled = np.hstack([np.linspace(0, 1, data_G2X.shape[0]), np.linspace(1.0, 1.0+np.sqrt(2.0), data_G2K2X.shape[0]), 
    np.linspace(1.0+np.sqrt(2.0), 1.0+np.sqrt(2.0)+np.sqrt(3.0)/2.0, data_G2L.shape[0])])
data_full = np.vstack((data_G2X[:, 3:], data_G2K2X[::-1, 3:], data_G2L[:, 3:]))
header = 'Col 1: plot friendly kpoints\nCol 2-4: Frequencies (meV)\nCol 5-7: Gamma (meV)'
np.savetxt('data/Al_phonons_phono3py_mesh%d%d%d_T%d' %(mesh, mesh, mesh, T), np.column_stack((k_scaled, data_full)), header=header)
