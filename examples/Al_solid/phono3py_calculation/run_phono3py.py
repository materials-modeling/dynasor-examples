import subprocess
import os
import glob

from ase.build import bulk
from ase.io import write

from general_utils.phonopy_helpers import write_vaspruns
from general_utils.lammps_calculators import get_single_calculator


def get_lattice_parameter(T):
    a = 4.04995569e+00
    b = 3.66366035e-05
    c = 5.89912719e-08
    d = -6.39165738e-11
    e = 7.39390742e-14
    f = -4.94782033e-17
    g = 1.61742997e-20

    return a + b*T + c*T**2 + d*T**3 + e*T**4 + f*T**5 + g*T**6


dim = 4
mesh = 40

potential_file = '/home/erikfr/projects/lammps_potentials/Al99.eam.alloy'
calc = get_single_calculator(potential_file, 'Al', 'eam/alloy')

Tvals = [900]


for T in Tvals:
    # setup primitive structure
    prim = bulk('Al', 'fcc', a=get_lattice_parameter(T))
    fname_prim = 'primitive_structures/Al.fcc.prim.T%d' % T
    write(fname_prim, prim, format='vasp')

    # generated displaced supercells
    subprocess.call('phono3py -d --dim=\"%d %d %d\"  -c %s' % (dim, dim, dim, fname_prim), shell=True)
    poscar_dir = 'poscars/T%d' % T
    if not os.path.exists(poscar_dir):
        os.makedirs(poscar_dir)
    else:
        subprocess.call('rm %s/*' % poscar_dir, shell=True)
    subprocess.call('mv POSCAR-* %s' % poscar_dir, shell=True)

    # compute forces and write vasprun-xxx.xml
    poscars = glob.glob('%s/POSCAR-*' % poscar_dir)
    vasprun_dir = 'vaspruns/T%d' % T
    if not os.path.exists(vasprun_dir):
        os.makedirs(vasprun_dir)
    else:
        subprocess.call('rm %s/*' % vasprun_dir, shell=True)

    write_vaspruns('%s/' % vasprun_dir, poscars, calc, translate=True)
    subprocess.call('phono3py --cf3 %s/vasprun.xml*' % vasprun_dir, shell=True)

    # compute fcs and write kappa file
    subprocess.call('phono3py --dim=\"%d %d %d" --sym_fc3r --sym_fc2 --tsym -c %s' % (dim, dim, dim, fname_prim), shell=True)

    subprocess.call('phono3py --dim=\"%d %d %d\" -c %s --mesh=\"%d %d %d\" --fc3 --fc2 --br --ts=\"%d\"' % (dim, dim, dim, fname_prim, mesh, mesh, mesh, T), shell=True)

    # save kappa data
    subprocess.call('mv kappa-m%d%d%d.hdf5 outputs/kappa-m%d%d%d-T%d.hdf5' % (mesh, mesh, mesh, mesh, mesh, mesh, T), shell=True)

    # remove crap
    files = ['disp_fc3.yaml', 'fc2.hdf5', 'fc3.hdf5', 'FORCES_FC3', 'SPOSCAR', 'phono3py_disp.yaml', 'phono3py.yaml']
    for f in files:
        os.remove(f)


"""
phono3py --dim="4 4 4" -c primitive_structures/Al.fcc.prim.T300 --mesh="12 12 12" --fc3 --fc2 --br --ts="300"
"""
