"""
fit dynasor data
"""

import numpy as np

from dynasor_pytools import DynasorAnalyzer
import matplotlib.pyplot as plt


def get_lattice_parameter(T):
    a = 4.04995569e+00
    b = 3.66366035e-05
    c = 5.89912719e-08
    d = -6.39165738e-11
    e = 7.39390742e-14
    f = -4.94782033e-17
    g = 1.61742997e-20

    return a + b*T + c*T**2 + d*T**3 + e*T**4 + f*T**5 + g*T**6


invfs2mev = 658.2119

# parameters
# ------------------------------------

size = 12
T = 900
a = get_lattice_parameter(T)
k1 = 2.0 * np.pi * 10 / a  # inverse nanometer
k2 = k1 / 2.0


# G2X
# -------------------------
dynasor_output = 'outputs/dynsf_outT%d_G2X.pickle' % T

kvec = np.array([k1, 0.0, 0.0])
kvals = np.linspace(0.0, np.linalg.norm(kvec), size + 1)[1:]  # skip gamma

analyzer = DynasorAnalyzer(dynasor_output)
analyzer.set_kvals(kvals=kvals)
analyzer.fit_currents()
G2X_l, G2X_t = analyzer.get_current_params()
G2X_l = np.vstack(([0, 0, 0, 0], G2X_l))  # add gamma point
G2X_t = np.vstack(([0, 0, 0, 0], G2X_t))  # add gamma point
k_G2X = np.linspace(0.0, 1.0, G2X_l.shape[0])

# G2K2X
# -------------------------
dynasor_output = 'outputs/dynsf_outT%d_G2K2X.pickle' % T

kvec = np.array([k1, k1, 0.0])
kvals = np.linspace(0.0, np.linalg.norm(kvec), size + 1)[1:]  # skip gamma

analyzer = DynasorAnalyzer(dynasor_output)
analyzer.set_kvals(kvals=kvals)
analyzer.fit_currents(npeaks=2)
G2K2X_l, G2K2X_t = analyzer.get_current_params()
G2K2X_l = np.vstack(([0, 0, 0, 0], G2K2X_l))  # add gamma point
G2K2X_t = np.vstack(([0, 0, 0, 0, 0, 0, 0], G2K2X_t))  # add gamma point
k_G2K2X = np.linspace(1.0, 1.0+np.sqrt(2.0), G2K2X_l.shape[0])
#analyzer.plot_currents(xlim=[0.0, 10000])
# G2L
# -------------------------
dynasor_output = 'outputs/dynsf_outT%d_G2L.pickle' % T

kvec = np.array([k2, k2, k2])
kvals = np.linspace(0.0, np.linalg.norm(kvec), size/2 + 1)[1:]  # skip gamma

analyzer = DynasorAnalyzer(dynasor_output)
analyzer.set_kvals(kvals=kvals)
analyzer.fit_currents()
G2L_l, G2L_t = analyzer.get_current_params()
G2L_l = np.vstack(([0, 0, 0, 0], G2L_l))  # add gamma point
G2L_t = np.vstack(([0, 0, 0, 0], G2L_t))  # add gamma point
k_G2L = np.linspace(1.0+np.sqrt(2.0), 1.0+np.sqrt(2.0)+np.sqrt(3.0)/2.0, G2L_l.shape[0])

# setup phonon dispersion
# --------------------------
data_l = np.vstack((G2X_l[:, 1:], G2K2X_l[::-1, 1:], G2L_l[:, 1:])) * invfs2mev
data_t1 = np.vstack((G2X_t[:, 1:], G2K2X_t[::-1, 1:4], G2L_t[:, 1:])) * invfs2mev
data_t2 = np.vstack((G2X_t[:, 1:], G2K2X_t[::-1, 4:7], G2L_t[:, 1:])) * invfs2mev

k = np.hstack((k_G2X, k_G2K2X, k_G2L))

# save data
# --------------
header = 'Col 1: plot friendly kpoint\nCol 2-4: LA  mode omega(meV), gamma(meV), prefac\nCol 5-7: TA1 mode omega(meV), gamma(meV), prefac\nCol 8-10: TA2 mode omega(meV), gamma(meV), prefac'
np.savetxt('data/dynasor_params_size%d_T%d' % (size, T), np.column_stack((k, data_l, data_t1, data_t2)), header=header, fmt='%8.5f')

# plot
# --------
plt.plot(k, data_l[:, 0], 'o')
plt.plot(k, data_t1[:, 0], 'o')
plt.plot(k, data_t2[:, 0], 'o')
plt.show()
