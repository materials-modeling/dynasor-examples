"""
Generate gnuplot heatmap data
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from dynasor_pytools import DynasorAnalyzer


invfs2mev = 658.2119
T = 300

wmax = 50.0

kshifts = [0.0, 1.0, 1.0 + np.sqrt(2.0)]
kscales = [1.0, np.sqrt(2.0), np.sqrt(3.0)/2.0]

f = open('data/current_heatmap_T%d' % T, 'w')
f.write('# Col 1: k-values (reduced units)\n')
f.write('# Col 2: omega (meV)\n')
f.write('# Col 3: Intensity (arbitrary units)\n')


# G2X
# --------------
dynasor_output = 'outputs/dynsf_outT%d_G2X.pickle' % T
analyzer = DynasorAnalyzer(dynasor_output)
k_vals = analyzer.raw_data_dict['k']
w_vals = analyzer.raw_data_dict['w'] * invfs2mev
w_cut = np.argmin(np.abs(w_vals-wmax))
w_vals = w_vals[:w_cut]
Cl = analyzer.raw_data_dict['Cl_k_w_0_0'][:w_cut, :]
Ct = analyzer.raw_data_dict['Ct_k_w_0_0'][:w_cut, :]
k_scaled = k_vals / np.max(k_vals) * kscales[0] + kshifts[0]
C_sum = Cl + Ct

for i, k in enumerate(k_scaled):
    for j, w in enumerate(w_vals):
        f.write('%12.8f   %12.8f   %12.8f\n' % (k, w, C_sum[j][i]))
    f.write('\n')


# G2K2X
# --------------
dynasor_output = 'outputs/dynsf_outT%d_G2K2X.pickle' % T
analyzer = DynasorAnalyzer(dynasor_output)
k_vals = analyzer.raw_data_dict['k']
w_vals = analyzer.raw_data_dict['w'] * invfs2mev
w_cut = np.argmin(np.abs(w_vals-wmax))
w_vals = w_vals[:w_cut]
Cl = analyzer.raw_data_dict['Cl_k_w_0_0'][:w_cut, :]
Ct = analyzer.raw_data_dict['Ct_k_w_0_0'][:w_cut, :]
k_scaled = k_vals / np.max(k_vals) * kscales[1] + kshifts[1]
C_sum = Cl + Ct

for i, k in enumerate(k_scaled):
    for j, w in enumerate(w_vals):
        f.write('%12.8f   %12.8f   %12.8f\n' % (k, w, C_sum[j][-i-1]))
    f.write('\n')


# G2L
# --------------
dynasor_output = 'outputs/dynsf_outT%d_G2L.pickle' % T
analyzer = DynasorAnalyzer(dynasor_output)
k_vals = analyzer.raw_data_dict['k']
w_vals = analyzer.raw_data_dict['w'] * invfs2mev
w_cut = np.argmin(np.abs(w_vals-wmax))
w_vals = w_vals[:w_cut]
Cl = analyzer.raw_data_dict['Cl_k_w_0_0'][:w_cut, :]
Ct = analyzer.raw_data_dict['Ct_k_w_0_0'][:w_cut, :]
k_scaled = k_vals / np.max(k_vals) * kscales[2] + kshifts[2]
C_sum = Cl + Ct

for i, k in enumerate(k_scaled):
    for j, w in enumerate(w_vals):
        f.write('%12.8f   %12.8f   %12.8f\n' % (k, w, C_sum[j][i]))
    f.write('\n')


f.close()
