"""
Compare dynasor and phono3py phonons
"""


import numpy as np
import matplotlib.pyplot as plt
from mplpub import setup, tableau

T = 300

mesh = 60
data_dynasor = np.loadtxt('data/dynasor_params_size12_T%d' % T)
data_phono3py = np.loadtxt('phono3py_calculation/data/Al_phonons_phono3py_mesh%d%d%d_T%d' % (mesh, mesh, mesh, T))

# plotting
extra_settings = {'legend.frameon': True,
                  'legend.fancybox': True,
                  'legend.framealpha': 1.0,
                  'legend.edgecolor': '0.8',
                  'legend.handlelength': 2.2}
setup(extra_settings=extra_settings)
figsize = (6.8, 4.8)
ms = 5
lw = 1.5
fs = 16

kpts = [0.0, 1.0, 1.0+np.sqrt(2.0)*0.25, 1.0+np.sqrt(2.0), 1.0+np.sqrt(2.0)+np.sqrt(3.0)/2.0]
kpts_labels = [r'$\UG{\Gamma}$', 'X', 'K', r'$\UG{\Gamma}$', 'L']

# phonon dispersion
fig = plt.figure(figsize=figsize)
plt.axvline(x=kpts[1], color='k', linewidth=0.9)
plt.axvline(x=kpts[2], color='k', linewidth=0.9, linestyle='--')
plt.axvline(x=kpts[3], color='k', linewidth=0.9)
plt.axvline(x=kpts[4], color='k', linewidth=0.9)

plt.plot(data_phono3py[:, 0], data_phono3py[:, 1], '-', color=tableau['red'], linewidth=lw, label='phono3py')
plt.plot(data_phono3py[:, 0], data_phono3py[:, 2], '-', color=tableau['red'], linewidth=lw)
plt.plot(data_phono3py[:, 0], data_phono3py[:, 3], '-', color=tableau['red'], linewidth=lw)

plt.plot(data_dynasor[:, 0], data_dynasor[:, 1], 'o', color=tableau['blue'], markersize=ms, label='dynasor')
plt.plot(data_dynasor[:, 0], data_dynasor[:, 4], 'o', color=tableau['blue'], markersize=ms)
plt.plot(data_dynasor[:, 0], data_dynasor[:, 7], 'o', color=tableau['blue'], markersize=ms)

plt.xlim([0.0, data_dynasor[-1, 0]])
plt.xlabel('Wave vector q', fontsize=fs)
plt.ylim([0.0, 45.0])
plt.ylabel(r'Frequency $\omega_0$ (meV)', fontsize=fs)
plt.xticks(kpts, kpts_labels, fontsize=fs)
plt.tick_params(axis='y', which='major', labelsize=fs)
plt.legend(loc=(0.55, 0.8), fontsize=fs-2, framealpha=1.0)
plt.tight_layout()
plt.savefig('eps/Al_phonon_dispersion_T%d.eps' % T, format='eps')


# phonon linewidth
fig = plt.figure(figsize=figsize)
plt.plot(data_phono3py[:, 0], 4*data_phono3py[:, 4], 'd', color=tableau['red'], linewidth=lw)
plt.plot(data_phono3py[:, 0], 4*data_phono3py[:, 5], 'x', color=tableau['red'], linewidth=lw)
plt.plot(data_phono3py[:, 0], 4*data_phono3py[:, 6], 's', color=tableau['red'], linewidth=lw)


plt.plot(data_dynasor[:, 0], data_dynasor[:, 2], 'o', color=tableau['blue'], markersize=ms)
plt.plot(data_dynasor[:, 0], data_dynasor[:, 5], 'o', color=tableau['blue'], markersize=ms)
plt.plot(data_dynasor[:, 0], data_dynasor[:, 8], 'o', color=tableau['blue'], markersize=ms)

plt.xlim([0.0, data_dynasor[-1, 0]])
plt.ylim(bottom=0.0)


# phonon linewidth only G2X, G2L
fig = plt.figure(figsize=figsize)

dynasor_G2X_inds = np.arange(13)
dynasor_G2L_inds = np.arange(7) + 26

phono3py_G2X_inds = np.arange(int(mesh/2)+1)
phono3py_G2L_inds = np.arange(int(mesh/2)+1) + mesh + 2

plt.axvline(x=1.0, color='k', linewidth=0.9)
lw = 1.4
ms = 6.0
# G2X
plt.plot(data_dynasor[dynasor_G2X_inds, 0], data_dynasor[dynasor_G2X_inds[::-1], 2], '--o', ms=ms, lw=lw, color=tableau['blue'], label='dynasor LA')
plt.plot(data_dynasor[dynasor_G2X_inds, 0], data_dynasor[dynasor_G2X_inds[::-1], 8], '--o', ms=ms, lw=lw, color=tableau['red'], label='dynasor TA')
plt.plot(data_phono3py[phono3py_G2X_inds, 0], 4*data_phono3py[phono3py_G2X_inds[::-1], 6], '-', lw=lw, color=tableau['blue'], label='phono3py LA') 
plt.plot(data_phono3py[phono3py_G2X_inds, 0], 4*data_phono3py[phono3py_G2X_inds[::-1], 4], '-', lw=lw, color=tableau['red'], label='phono3py TA') 

# G2L
plt.plot(data_dynasor[dynasor_G2L_inds, 0]-np.sqrt(2.0), data_dynasor[dynasor_G2L_inds, 2], '--o', ms=ms, lw=lw, color=tableau['blue'])
plt.plot(data_dynasor[dynasor_G2L_inds, 0]-np.sqrt(2.0), data_dynasor[dynasor_G2L_inds, 8], '--o', ms=ms, lw=lw, color=tableau['red'])
plt.plot(data_phono3py[phono3py_G2L_inds, 0]-np.sqrt(2.0), 4*data_phono3py[phono3py_G2L_inds, 6], '-', lw=lw, color=tableau['blue']) 
plt.plot(data_phono3py[phono3py_G2L_inds, 0]-np.sqrt(2.0), 4*data_phono3py[phono3py_G2L_inds, 4], '-', lw=lw, color=tableau['red']) 


plt.xlim([0.0, np.sqrt(3.0)])
plt.ylim(bottom=0.0)

plt.xticks([0.0, 1.0, 1.0+np.sqrt(3.0)/2.0], ['X', r'$\UG{\Gamma}$', 'L'], fontsize=fs)
plt.xlabel('Wave vector q', fontsize=fs)
plt.ylabel(r'Damping $\UG{\Gamma}$ (mev)', fontsize=fs)
plt.tick_params(axis='y', which='major', labelsize=fs)

plt.legend(fontsize=fs-2, ncol=2, loc=0)
plt.tight_layout()
plt.savefig('eps/Al_phonon_damping_T%d.eps' % T, format='eps')
plt.show()
