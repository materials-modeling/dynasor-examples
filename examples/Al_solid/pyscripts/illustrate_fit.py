"""
Illustrate how fitting looks like
"""

import numpy as np
import matplotlib.pyplot as plt
from mplpub import setup, tableau

from dynasor_pytools import DynasorAnalyzer
from dynasor_pytools.dynasor_analyzer import fit
from dynasor_pytools.peak_functions import function_Fkt, function_Fkt_double, function_Ckw, function_Ckw_double
from dynasor_pytools.fft_helpers import filon_fft


invfs2mev = 658.2119


def get_lattice_parameter(T):
    a = 4.04995569e+00
    b = 3.66366035e-05
    c = 5.89912719e-08
    d = -6.39165738e-11
    e = 7.39390742e-14
    f = -4.94782033e-17
    g = 1.61742997e-20

    return a + b*T + c*T**2 + d*T**3 + e*T**4 + f*T**5 + g*T**6


# parameters
# ------------------------------------

size = 12
T = 900
a = get_lattice_parameter(T)
k1 = 2.0 * np.pi * 10 / a  # inverse nanometer
k2 = k1 / 2.0

kind = 5


# Load G2K2X
# -------------------------
dynasor_output = 'outputs/dynsf_outT%d_G2K2X.pickle' % T

kvec = np.array([k1, k1, 0.0])
kvals = np.linspace(0.0, np.linalg.norm(kvec), size + 1)[1:]  # skip gamma

analyzer = DynasorAnalyzer(dynasor_output)
analyzer.set_kvals(kvals=kvals)
analyzer.fit_currents(npeaks=2)
params_l, params_t = analyzer.get_current_params()

t, Cl_all, Ct_all = analyzer.get_currents()

Cl = Cl_all[:, kind]
Ct = Ct_all[:, kind]

w_l, Cl_w = filon_fft(t, Cl, fft_window='fermi-dirac', t_0=1000, t_width=100)
w_t, Ct_w = filon_fft(t, Ct, fft_window='fermi-dirac', t_0=2500, t_width=300)
w_l *= invfs2mev
w_t *= invfs2mev

# Fitting
# ----------
params_Cl = fit(t, Cl, function_Fkt, params_guess=[0.015, 0.0015, Cl[0]])['params']
params_Ct = fit(t, Ct, function_Fkt_double, npeaks=2,
                params_guess=[0.015, 0.0015, Cl[0], 0.01, 0.001, Cl[0]])['params']


# Plotting
# ------------------
extra_settings = {'legend.frameon': True,
                  'legend.fancybox': True,
                  'legend.framealpha': 0.8,
                  'legend.edgecolor': '0.8'}
setup(extra_settings=extra_settings)
fig = plt.figure(figsize=(14.2, 6.2))

ms = 4
fs = 16
xlim1 = [0, 1000]
ylim1 = [-0.3, 0.35]
xlim2 = [0, 50]
ylim2 = [0, 95]

ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 2)
ax3 = fig.add_subplot(2, 2, 3)
ax4 = fig.add_subplot(2, 2, 4)


t_lin = np.linspace(np.min(t), np.max(t), 10000)
w_lin = np.linspace(np.min(w_l), np.max(w_l), 10000)
Cl_w_fit = function_Ckw(w_lin/invfs2mev, *params_Cl)
w0, b, A = params_Cl
Cl_max_value = np.max(Cl_w_fit)

# indicate exponetial decay
ax1.plot(t_lin, A * np.exp(-b*t_lin/2.0), '--k', dashes=(6, 3))
ax1.text(200, 0.18, r'$\mathrm{e}^{-\UG{\Gamma} t/2}$', fontsize=fs+4)

# plot all
ax1.plot(t_lin, function_Fkt(t_lin, *params_Cl), '-', color=tableau['red'], markersize=ms, label='fit')
ax1.plot(t, Cl, 'o', color=tableau['blue'], markersize=ms, label='data')

ax2.plot(t_lin, function_Fkt_double(t_lin, *params_Ct), '-', color=tableau['red'], markersize=ms, label='fit')
ax2.plot(t, Ct, 'o', color=tableau['blue'], markersize=ms, label='data')

ax3.plot(w_lin, Cl_w_fit, '-', color=tableau['red'])
ax3.plot(w_l, Cl_w, 'o', color=tableau['blue'], markersize=ms)

ax4.plot(w_lin, function_Ckw_double(w_lin/invfs2mev, *params_Ct), '-', color=tableau['red'])
ax4.plot(w_t, Ct_w, 'o', color=tableau['blue'], markersize=ms)


# indicate Full Width at half max
ax3.annotate(s='', xy=((w0-b/2) * invfs2mev, Cl_max_value / 2.0), xytext=((w0+b/2) * invfs2mev, Cl_max_value / 2.0), arrowprops=dict(arrowstyle='<->'))
ax3.text(w0*invfs2mev-0.3, Cl_max_value/2-10.5, r'$\UG{\Gamma}$', fontsize=fs+4)

for ax in [ax1, ax2]:
    ax.set_xlabel('Time (fs)', fontsize=fs)
    ax.set_xlim(xlim1)
    ax.set_ylim(ylim1)
    ax.legend(fontsize=fs, loc=1)

for ax in [ax3, ax4]:
    ax.set_xlabel('Frequency (meV)', fontsize=fs)
    ax.set_xlim(xlim2)
    ax.set_ylim(ylim2)

for ax in [ax1, ax2, ax3, ax4]:
    ax.tick_params(labelsize=fs)

ax1.set_ylabel('$C(q,t)$', fontsize=fs)
ax3.set_ylabel('$C(q,\omega)$', fontsize=fs)

ax1.set_title('Longitudinal', fontsize=fs)
ax2.set_title('Transverse', fontsize=fs)

plt.tight_layout()
plt.savefig('eps/fit_illustration.eps', format='eps')
plt.show()
