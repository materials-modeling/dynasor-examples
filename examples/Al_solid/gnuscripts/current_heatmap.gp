res
set term postscript eps enh col sol "Helvetica" 22
#set term png

T = 300
data_file = 'data/current_heatmap_T'.T
data_phonopy = 'phono3py_calculation/data/Al_phonons_phono3py_mesh484848_T'.T
filename='eps/current_heatmap_T'.T.'.eps'
set out filename
set enc iso_8859_1

# Shift for k-vector
# =====================
a1 = 1.0
a2 = a1 + 0.25*sqrt(2)
a3 = a1 + sqrt(2)
a4 = a3 + sqrt(3)/2
wMax = 45.0
set arrow from a1,0 to a1,wMax nohead front
set arrow from a2,0 to a2,wMax nohead front lt 0 lc rgb "black"
set arrow from a3,0 to a3,wMax nohead front
set arrow from a4,0 to a4,wMax nohead front
# Colorbar
# =====================
set palette color
set palette model RGB
set palette defined ( \
  20   '#ffffcc', \
  35   '#a1dab4', \
  45   '#41b6c4', \
  55   '#2c7fb8', \
  80   '#253494' )
#set palette defined ( \
  20   '#d7191c', \
  25   '#fdae61', \
  30   '#ffffbf', \
  40   '#abdda4', \
  80   '#2b83ba' )
#set cbti 0,0.05
unset cbti
set cbra [0:150]
#unset colorbox

set view map
set pm3d map interpolate 2,2
#set cntrparam bspline
#set cntrparam points 10
#set cntrparam levels discrete 0.01,0.03,0.1
#set contour surface


# Plot
# ========================
set lmargin at screen 0.12;
set rmargin at screen 0.90;
set bmargin at screen 0.15;
set tmargin at screen 0.95;

set style line 1 lc rgb '#e41a1c' lt 1 lw 2 pt 7 ps 2.0 # red


set xtics('{/Symbol G}' 0.0,'X' a1,'K' a2,'{/Symbol G}' a3,'L' a4)
set xla 'Wave vector k' 
set yla 'Frequency (meV)'
set yra[0:wMax]
set key at a3-0.35,45
set key samplen 2.5
splot data_file u 1:2:3 w pm3d,\
data_phonopy u 1:2:(0.0) w l ls 1 t "phono3py",\
   "" u 1:3:(0.0) w l ls 1 t "",\
   "" u 1:4:(0.0) w l ls 1 t ""