#!/bin/sh

DYNSF=dynsf

K_BINS=49
K_POINTS=49
TIME_WINDOW=1000    # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=300000   # Read at most MAX_FRAMES frames from trajectory file (then stop)
dt=$((5*2)) # This needs to be correspond to lammps timestep * dumpFreq in fs.



# T = 300
# a = 4.11200710941167
#
# K - points
# 
# G2X =   [0 0 0] -> [1 0 0]
# G2K2X = [0 0 0] -> [0.75 0.75 0] -> [1 1 0]
# G2L =   [0 0 0] -> [0.5 0.5 0.5]
#


k900_1=15.2800934920527    # 2pi / a      (inverse nm)
k900_2=7.64004674602637    # 2pi / a / 2  (inverse nm)


TRAJECTORY="/scratch/erikfr/data_storage/Al_data/dynasor_runs/size12_T900/pos.data.gz"


OUTPUT="outputs/dynsf_outT900_G2X"
echo "\nRunning Gamma to X\n\n"
${DYNSF} -f "$TRAJECTORY" \
    --k-bins=$K_BINS \
    --k-points=$K_POINTS \
    --max-frames=$MAX_FRAMES \
    --op=$OUTPUT.pickle \
    --nt=$TIME_WINDOW \
    --dt=$dt \
    --k-sampling="line" \
    --k-direction=$k900_1,0,0


OUTPUT="outputs/dynsf_outT900_G2K2X"
echo "\nRunning Gamma to K to X\n\n"
${DYNSF} -f "$TRAJECTORY" \
    --k-bins=$K_BINS \
    --k-points=$K_POINTS \
    --max-frames=$MAX_FRAMES \
    --op=$OUTPUT.pickle \
    --nt=$TIME_WINDOW \
    --dt=$dt \
    --k-sampling="line" \
    --k-direction=$k900_1,$k900_1,0


OUTPUT="outputs/dynsf_outT900_G2L"
echo "\nRunning Gamma to L\n\n"
${DYNSF} -f "$TRAJECTORY" \
    --k-bins=$K_BINS \
    --k-points=$K_POINTS \
    --max-frames=$MAX_FRAMES \
    --op=$OUTPUT.pickle \
    --nt=$TIME_WINDOW \
    --dt=$dt \
    --k-sampling="line" \
    --k-direction=$k900_2,$k900_2,$k900_2
