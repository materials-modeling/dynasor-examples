dynasor-examples
=================

Collection of nice and clean dynasor examples with all the necessary scripts to reproduce the results.     


<div class="notecard note">
  <p><strong>Note:</strong> The scripts in `examples` were used with old version of dynasor, dynasor 1.0, from 2021 and will thus not work with dynasor 2.0. </p>
</div>
