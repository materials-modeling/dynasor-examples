"""
Dynasor analyzing classes/tools
"""


import pickle
import numpy as np
import matplotlib.pyplot as plt


invfs2mev = 658.2119  # angular frequency


class DynasorCurrentAnalyzer:
    """
    Analyzer for currents in time domain
    """

    def __init__(self, dynasor_pickle):
        self.dynasor_pickle = dynasor_pickle
        self._parse_pickle()
        self.fit_dict = {}

        try:
            self.time = self.raw_data_dict['t']   # fs
            self.omega = self.raw_data_dict['w']   # angular inverse fs
        except KeyError:
            raise

    def _parse_pickle(self):
        dynasor_pickle = pickle.load(open(self.dynasor_pickle, 'rb'),
                                     encoding='latin1')
        raw_data_dict = dict()
        for data in dynasor_pickle:
            raw_data_dict[data[1]] = data[0]
        self.raw_data_dict = raw_data_dict

    def set_kvals(self, kvals=None, tol=1e-6):
        if kvals is None:
            self.kvals = self.raw_data_dict['k']  # use all
            self.kinds = list(range(len(self.kvals)))
        else:
            kinds = []
            for kval in kvals:
                try:
                    kind = np.where(np.abs(self.raw_data_dict['k'] - kval) < tol)[0][0]
                    kinds.append(kind)
                except IndexError:
                    print('No match for kval', kval)
                    raise
            self.kinds = kinds
            self.kvals = kvals

    @property
    def n_kvals(self):
        return len(self.kinds)

    def get_time_currents(self, partial='0_0', t_max=None):
        """ Return raw data for currents in time domain """
        if t_max is None:
            t_max = np.max(self.time)+1

        Cl_kt = self.raw_data_dict['Cl_k_t_' + partial][:, self.kinds]
        Ct_kt = self.raw_data_dict['Ct_k_t_' + partial][:, self.kinds]

        time = self.time[self.time <= t_max]
        Cl_kt = Cl_kt[self.time <= t_max, :]
        Ct_kt = Ct_kt[self.time <= t_max, :]

        assert Cl_kt.shape[1] == self.n_kvals
        assert Ct_kt.shape[1] == self.n_kvals

        return time, Cl_kt, Ct_kt

    def get_frequency_currents(self, partial='0_0', w_max=None):
        """ Return raw data for currents in time domain """
        if w_max is None:
            w_max = np.max(self.omega)+1

        Cl_kt = self.raw_data_dict['Cl_k_w_' + partial][:, self.kinds]
        Ct_kt = self.raw_data_dict['Ct_k_w_' + partial][:, self.kinds]

        omega = self.omega[self.omega <= w_max]
        Cl_kt = Cl_kt[self.omega <= w_max, :]
        Ct_kt = Ct_kt[self.omega <= w_max, :]

        assert Cl_kt.shape[1] == self.n_kvals
        assert Ct_kt.shape[1] == self.n_kvals

        return omega, Cl_kt, Ct_kt

    def fit_currents(self, partial='0_0', npeaks=1):
        """ Fitting function. """
        raise NotImplementedError
        time, Cl_kt, Ct_kt = self.get_time_currents()

        Cl_kt_data = []
        Ct_kt_data = []
        for kval, Cl_t, Ct_t in zip(self.kvals, Cl_kt.T, Ct_kt.T):
            fit_Cl_t = peak_fit(time, Cl_t, function_Fkt)
            fit_Cl_t['kval'] = kval
            Cl_kt_data.append(fit_Cl_t)

            if npeaks == 1:
                fit_Ct_t = peak_fit(time, Ct_t, function_Fkt)
            elif npeaks == 2:
                fit_Ct_t = peak_fit(time, Ct_t, function_Fkt_double, 2)

            else:
                raise ValueError('Currently does not support %d fits' % npeaks)
            fit_Ct_t['kval'] = kval
            Ct_kt_data.append(fit_Ct_t)

        self.fit_dict['Cl_k_t_' + partial] = Cl_kt_data
        self.fit_dict['Ct_k_t_' + partial] = Ct_kt_data

    def plot_currents(self, partial='0_0', t_max=5000):
        time, Cl_kt, Ct_kt = self.get_time_currents(partial=partial, t_max=t_max)

        fig = plt.figure(figsize=(14, 12), dpi=100, facecolor='w', edgecolor='k')
        axes1 = [fig.add_subplot(self.n_kvals, 2, 2*i+1) for i in range(self.n_kvals)]
        axes2 = [fig.add_subplot(self.n_kvals, 2, 2*i+2) for i in range(self.n_kvals)]

        for row, ax in enumerate(axes1):
            ax.plot(time, Cl_kt[:, row], '-or', ms=4)

        for row, ax in enumerate(axes2):
            ax.plot(time, Ct_kt[:, row], '-or', ms=4)

        plt.tight_layout()
        plt.show()

    def plot_currents_with_fits(self, partial='0_0', t_max=5000):
        Cl_kt_data = self.fit_dict['Cl_k_t_' + partial]
        Ct_kt_data = self.fit_dict['Ct_k_t_' + partial]

        fig = plt.figure(figsize=(14, 12), dpi=100, facecolor='w', edgecolor='k')
        axes1 = [fig.add_subplot(self.n_kvals, 2, 2*i+1) for i in range(self.n_kvals)]
        axes2 = [fig.add_subplot(self.n_kvals, 2, 2*i+2) for i in range(self.n_kvals)]

        for ax, Cl_dict in zip(axes1, Cl_kt_data):
            ax.plot(Cl_dict['x-data'], Cl_dict['y-data'], '.r', label='data')
            ax.plot(Cl_dict['x-fit'], Cl_dict['y-fit'], 'k', label='fit')
            ax.set_xlim([0, t_max])

        for ax, Ct_dict in zip(axes2, Ct_kt_data):
            ax.plot(Ct_dict['x-data'], Ct_dict['y-data'], '.r', label='data')
            ax.plot(Ct_dict['x-fit'], Ct_dict['y-fit'], 'k', label='fit')
            ax.set_xlim([0, t_max])

        plt.legend(loc=1)
        plt.tight_layout()
        plt.show()

    def get_fit_params(self, partial='0_0', append_gamma=False):
        """ Return fitted parameters """

        # collect parameters
        params_l = []
        for kval, Cl_dict in zip(self.kvals, self.fit_dict['Cl_k_t_' + partial]):
            params_l.append([kval, *Cl_dict['params']])
        params_l = np.array(params_l)

        params_t = []
        for kval, Ct_dict in zip(self.kvals, self.fit_dict['Ct_k_t_' + partial]):
            params_t.append([kval, *Ct_dict['params']])
        params_t = np.array(params_t)

        if append_gamma:
            gamma_l = np.array([0] * params_l.shape[1]).reshape(1, -1)
            params_l = np.vstack((gamma_l, params_l))
            gamma_t = np.array([0] * params_t.shape[1]).reshape(1, -1)
            params_t = np.vstack((gamma_t, params_t))

        return params_l, params_t

    def set_longitudinal_current_fits(self, fits, partial='0_0'):
        """ Set list of fit dits for each kinds """
        assert len(fits) == self.n_kvals
        self.fit_dict['Cl_k_t_' + partial] = fits

    def set_transverse_current_fits(self, fits, partial='0_0'):
        """ Set list of fit dits for each kinds """
        assert len(fits) == self.n_kvals
        self.fit_dict['Ct_k_t_' + partial] = fits
