import numpy as np
from scipy.optimize import curve_fit
from .damped_oscillator_functions import function_Ckt, function_Ckt_double


time_functions = {1: function_Ckt,
                  2: function_Ckt_double}


def fit_time_oscillator(x, y, n_modes=1, params_guess=None,
                        low_bounds=None, high_bounds=None, **scipy_kwargs):

    if n_modes in [1, 2]:
        function = time_functions[n_modes]
    else:
        raise ValueError

    # starting params
    if params_guess is None:
        params_guess = [8.0/700, 2.0/700, y[0]/n_modes]*n_modes
        params_guess[0] *= 1.5
        params_guess[1] *= 1.1
        params_guess[2] *= 1.1

    # bounds
    if low_bounds is None:
        low_bounds = [1/700.0, 0.0, 0.1*y[0]] * n_modes
    if high_bounds is None:
        high_bounds = [50.0/700.0, 20.0/700, 10*y[0]] * n_modes

    # sanity check starting params
    for p, l, h in zip(params_guess, low_bounds, high_bounds):
        if p < l:
            raise ValueError('params too low {} < {}'.format(p, l))
        if p > h:
            raise ValueError('params too high {} > {}'.format(p, h))

    # fit
    params, _ = curve_fit(function, x, y, p0=params_guess,
                          bounds=(low_bounds, high_bounds),
                          method='dogbox', maxfev=10000, **scipy_kwargs)

    # if more than one peak, sort by peak frequency
    if n_modes > 1.5:
        if params[0] > params[3]:
            params = np.hstack((params[3:6], params[0:3]))

    # collect results into a fit_dict
    x_fit = np.linspace(np.min(x), np.max(x), 5000)
    y_fit = function(x_fit, *params)
    fit_dict = {'x-data': x,
                'y-data': y,
                'x-fit': x_fit,
                'y-fit': y_fit,
                'params': params,
                'n_modes': n_modes,
                'function': function.__name__}

    return fit_dict
