import numpy as np


def function_Ckt(t, w0, b, A):
    """ Single C(k, t) function """
    if w0 > b/2.0:  # weak damping
        return _function_Ckt_underdamped(t, w0, b, A)
    else:  # overdamped
        print('Warning: Overdamped region')
        return _function_Ckt_overdamped(t, w0, b, A)


def function_Ckt_double(t, w01, b1, A1, w02, b2, A2):
    """ Two C(k, t) functions """
    return function_Ckt(t, w01, b1, A1) + function_Ckt(t, w02, b2, A2)


def _function_Ckt_underdamped(t, w0, b, A):
    """ under damped C(k, t)
    C(k,t) = A * exp(-bt/2) * [ cos(w_0*t) + b/(2w_e)sin(w_e*t)
    """
    we = np.sqrt(w0**2 - b**2/4.0)
    return A * np.exp(-b*np.abs(t)/2.0) * (np.cos(we*t) - 0.5*b/we*np.sin(we*np.abs(t)))


def _function_Ckt_overdamped(t, w0, b, A):
    """ over damped C(k, t) """
    we = np.sqrt(b**2/4.0 - w0**2)
    return A * np.exp(-b*np.abs(t)/2.0) * (np.cosh(we*t) - 0.5*b/we*np.sinh(we*np.abs(t)))


def function_Ckw(w, w0, b, A):
    """
    Single peak function C(k, w)
    C(k, w) = x^2 * A * G/( (x^2 - x_0^2)^2 + (Gx)^2 )
    """
    return 2 * w**2 * A * b / ((w**2 - w0**2)**2 + (w*b)**2)


def function_Ckw_double(w, w01, b1, A1, w02, b2, A2):
    """
    Double peak function C(k, w)
    """
    return function_Ckw(w, w01, b1, A1) + function_Ckw(w, w02, b2, A2)
