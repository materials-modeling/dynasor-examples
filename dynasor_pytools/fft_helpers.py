from dsf import filon
import numpy as np


def fermi_dirac(t, t_0=1000, t_width=100):
    return 1.0 / (np.exp((t-t_0)/t_width) + 1)


def filon_fft(time, time_series, fft_window=None, **window_kwargs):
    ''' FFT via dynasor filon integration function'''

    if len(time_series) % 2 == 0:
        time_series = np.delete(time_series, -1)
        time = np.delete(time, -1)

    if fft_window == 'fermi-dirac':
        window = fermi_dirac(time, **window_kwargs)
        w, time_series_fft = filon.fourier_cos(time_series*window, time[1] - time[0])
    elif fft_window is None:
        w, time_series_fft = filon.fourier_cos(time_series, time[1] - time[0])
    else:
        raise ValueError('FFT window not recognized')

    return w, time_series_fft
