"""

Collection of utility functions

"""


def write_gnuplot_heatmap_data(fname, x, y, Z, header=None):
    """
    Writes gnuplottable heatmap datafile.

    Parameters
    -----------
    fname : str
        filename
    x : array
        one dimensional array with x values
    y : array
        one dimensional array with y values
    z : array
        two dimensional array with shape (len(y), len(x))

    """

    f = open(fname, 'w')
    if header is not None:
        f.write(header+'\n')

    for i, x_i in enumerate(x):
        for j, y_i in enumerate(y):
            f.write('%12.8f   %12.8f   %12.8f\n' % (x_i, y_i, Z[j][i]))
        f.write('\n')
    f.close()
