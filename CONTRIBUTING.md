Contribution guidelines
=======================

Please use spaces
-----------------

While you are entitled to [your own
opinion](http://lea.verou.me/2012/01/why-tabs-are-clearly-superior/) this
project uses spaces instead of tabs. Even if you are geeky enough to care and
like [Silicon Valley](https://www.youtube.com/watch?v=SsoOG6ZeyUI) you should
know that [developers who use spaces make more
money](https://stackoverflow.blog/2017/06/15/developers-use-spaces-make-money-use-tabs/).
Also the use of spaces is strongly recommended by our beloved
[pep8](https://www.python.org/dev/peps/pep-0008/) standard.


Commits/issues/merge requests
-----------------------------

When writing commit messages, generating issues, or submitting merge
requests, please use the following codes to identify the category of
your commit/issue/merge request.

* BLD: change related to building  
* BUG: bug fix
* DATA: general data
* DOC: documentation  
* ENH: enhancement  
* MAINT: maintenance commit (refactoring, typos, etc.)  
* STY: style fix (whitespace, PEP8)  
* TST: addition or modification of tests  
* REL: related to releases  

Less common:
* API: an (incompatible) API change  
* DEP: deprecate something, or remove a deprecated object  
* DEV: development tool or utility  
* FIG: images and figures
* REV: revert an earlier commit  

The first line should not exceed 78 characters. If you require more
space, insert an empty line after the "title" and add a longer message
below. In this message, you should again limit yourself to 78
characters *per* line.

Hint: If you are using emacs you can use ``Meta``+``q``
[shortcut](https://shortcutworld.com/en/Emacs/23.2.1/linux/all) to
"reflow the text". In sublime you can achieve a similar effect by using
``Alt``+``q`` (on Linux)/ ``Alt``+``Cmd``+``q`` (on MacOSX).
